package com.example.liteprisma


class ImageStyle {
    var job_id=""
    var output_url=""
    var err=""

    fun getError(): String {
        return err
    }

    fun getJobId(): String {
        return job_id
    }

    fun getOutputUrl(): String {
        return output_url
    }

    override fun toString(): String {
        return job_id + " ||| " + output_url
    }
}