package com.example.liteprisma


class ImageColorize {
    var job_id=""
    var output_url=""

    fun getJobId(): String {
        return job_id
    }

    fun getOutputUrl(): String {
        return output_url
    }

    override fun toString(): String {
        return job_id + " ||| " + output_url
    }
}