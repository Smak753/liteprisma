package com.example.liteprisma

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_change.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL


class Change : AppCompatActivity() {
    val pickImage = 100
    var editedPhotoURL: String? = null
    var originalPhotoUri: Uri? = null
   lateinit var imageUri:Uri
    lateinit var imageStyle: Uri
    var type=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change)
        setSupportActionBar(include as Toolbar?)
        val imageUri = intentUri(intent)
        showImage(imageUri)
        originalPhotoUri=imageUri

        clorize_btn.setOnClickListener({
            colorize(intentUri(intent))
        })
        style_btn.setOnClickListener({
            mixStyles(intentUri(intent))
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == pickImage) {
//                val contentPhoto = data.extras["content_photo"] as Uri
                imageStyle = data.data
                imageStyle(imageUri, imageStyle)
            }
        }
    }

    private fun intentUri(intent: Intent): Uri {
        return intent.extras["image"] as Uri
    }
    private fun showImage(imageUri: Uri?) {
            val imageStream = contentResolver.openInputStream(imageUri)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            photo.setImageBitmap(selectedImage)
        }
    fun save(){
        if (editedPhotoURL == null) {
            Toast.makeText(applicationContext, "nothing for save", Toast.LENGTH_SHORT).show()
        } else {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

            StrictMode.setThreadPolicy(policy)

            val url = URL(editedPhotoURL)
            val bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())

            val bytes = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes)


            val f = File(Environment.getExternalStorageDirectory().toString() + File.separator + "test.jpg")
            f.createNewFile()

            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())

            fo.close()
            Toast.makeText(applicationContext, "Image saved", Toast.LENGTH_SHORT).show()

        }
    }
    fun show(type:Boolean){
        if (type) {
            if (editedPhotoURL != null) {
                showImage(originalPhotoUri)
                this.type = false
            }
        } else {
            showImage(originalPhotoUri)
            this.type = true
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save -> {
                save()
                true
            }
            R.id.show -> {
                show(type)

                true
            }
            else-> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }
    private fun colorize(imgUri:Uri) {
        var file = File(FileUtils.getPath(this, imgUri))
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)

        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.deepai.org/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val service = retrofit.create(Connect::class.java)

        val resultCall: Call<ImageColorize> = service.postImage(body)

        resultCall.enqueue(object : Callback<ImageColorize> {
            override fun onFailure(call: Call<ImageColorize>?, t: Throwable) {
                Log.e("PH onFailure", t.message)
                Toast.makeText(applicationContext, "Image upload failed", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ImageColorize>?, response: Response<ImageColorize>?) {
                Log.d("PH code", response!!.code().toString())
                Log.d("PH Upload", "success")
                Log.d("PH Response", response.toString())

                Picasso.with(applicationContext).load(response.body()!!.getOutputUrl()).into(photo)
                editedPhotoURL = response.body()!!.getOutputUrl()

            }
        })
    }
    private fun mixStyles(imageUri: Uri) {

        val openGalleryIntent = Intent()
        openGalleryIntent.type = "image/*"
        openGalleryIntent.action = Intent.ACTION_GET_CONTENT

        this.imageUri = imageUri
        startActivityForResult(Intent.createChooser(openGalleryIntent, "Select photo"), pickImage)


//
    }
    private fun imageStyle(imageUri:Uri,styleUri: Uri){
        val contentFile = File(FileUtils.getPath(applicationContext, imageUri))
        Log.v("PH contentFile", contentFile.toString())

        val styleFile = File(FileUtils.getPath(applicationContext, styleUri))
        Log.v("PH styleFile", styleFile.toString())

        val requestContentFile = RequestBody.create(MediaType.parse("multipart/form-data"), contentFile)
        val requestStyleFile = RequestBody.create(MediaType.parse("multipart/form-data"), styleFile)

        val contentBody = MultipartBody.Part.createFormData("image", contentFile.name, requestContentFile)
        val styleBody = MultipartBody.Part.createFormData("image", styleFile.name, requestStyleFile)


        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.deepai.org/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val service = retrofit.create(Connect::class.java)

        val resultCall: Call<ImageStyle> = service.stylizeImage(contentBody, styleBody)

        resultCall.enqueue(object : Callback<ImageStyle> {
            override fun onFailure(call: Call<ImageStyle>?, t: Throwable) {
                Log.e("PH onFailure", t.message)
                Toast.makeText(applicationContext, "Image upload failed", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ImageStyle>?, response: Response<ImageStyle>?) {
                Log.d("PH code", response!!.code().toString())
                Log.d("PH Upload", "success")
                Log.d("PH Response", response.toString())
                if (response.body()!!.getError() == "") {
                    Picasso.with(applicationContext).load(response.body()!!.getOutputUrl()).into(photo)
                    editedPhotoURL = response.body()!!.getOutputUrl()
                } else {
                    Toast.makeText(applicationContext, response.body()!!.getError(), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
