package com.example.liteprisma

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface Connect {
    @Headers("Api-Key:90c6e47d-ae94-491c-93c3-9594d1fa956a")
    @Multipart
    @POST("api/colorizer")
    fun postImage(
            @Part image: MultipartBody.Part
    ): Call<ImageColorize>

    @Headers("Api-Key:90c6e47d-ae94-491c-93c3-9594d1fa956a")
    @Multipart
    @POST("api/neural-style")
    fun stylizeImage(
            @Part content: MultipartBody.Part,
            @Part style: MultipartBody.Part
    ) : Call<ImageStyle>
}